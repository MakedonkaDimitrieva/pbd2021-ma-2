package si.uni_lj.fri.pbd.miniapp2

import android.app.*
import android.content.Intent
import android.graphics.Color
import android.media.MediaPlayer
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import kotlin.random.Random.Default.nextInt

class MediaPlayerService : Service() {

    companion object {
        private val TAG: String? = MediaPlayerService::class.simpleName
        const val ACTION_START = "start_service"
        const val ACTION_PLAY = "play_service"
        const val ACTION_PAUSE = "pause_service"
        const val ACTION_STOP = "stop_service"
        const val ACTION_EXIT = "exit_service"
        private const val channelID = "background_player"
        const val NOTIFICATION_ID = 21
    }

    var isMediaPlayerRunning: Boolean = false
    var mediaPlayer: MediaPlayer = MediaPlayer()
    var pause: Boolean = false
    var title: String = ""

    override fun onCreate() {
        Log.d(TAG, "Creating service");
        createNotificationChannel()
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.d(TAG, "Starting service")

        if (intent.action == ACTION_PLAY) {
            playSong()
        }
        if (intent.action == ACTION_PAUSE) {
            pauseSong()
        }
        if (intent.action == ACTION_STOP) {
            stopSong()
        }
        if (intent.action == ACTION_EXIT) {
            exit()
        }
        return START_STICKY
    }

    inner class RunServiceBinder : Binder() {
        val service: MediaPlayerService
            get() = this@MediaPlayerService
    }

    // defining serviceBinder and instantiating it to RunServiceBinder
    private val serviceBinder = RunServiceBinder()

    override fun onBind(intent: Intent): IBinder {
        Log.d(TAG, "Binding service")
        return serviceBinder
    }

    fun foreground() {
        startForeground(NOTIFICATION_ID, createNotification())
    }

    fun background() {
        stopForeground(true)
    }

    var songs = arrayOf("changes.mp3", "good-news.mp3", "happy-birthday.mp3", "spring-flower.mp3", "travel-dreams.mp3")

    fun playSong() {
        if (pause) {
            mediaPlayer.seekTo(mediaPlayer.currentPosition)
            mediaPlayer.start()
            pause = false
            isMediaPlayerRunning = true
            Log.d(TAG, "Playing song after pause")
        } else {
            isMediaPlayerRunning = true
            var random: Int = nextInt(0, 4)
            var assetFileDescriptor = assets.openFd(songs[random])
            mediaPlayer.setDataSource(assetFileDescriptor.fileDescriptor)
            mediaPlayer.prepare()
            mediaPlayer.start()
            title = songs[random]
            Log.d(TAG, "Playing song from beginning")
        }
    }

    fun pauseSong() {
        if(mediaPlayer.isPlaying) {
            mediaPlayer.pause()
            pause = true
            Log.d(TAG, "Pausing song")
        }
    }

    fun stopSong() {
        if(mediaPlayer.isPlaying || pause) {
            pause = false
            mediaPlayer.stop()
            mediaPlayer.reset()
            //mediaPlayer.release()
            mediaPlayer = MediaPlayer()
            Log.d(TAG, "Stopping song")
        }
    }

    fun exit() {
        if(mediaPlayer.isPlaying) {
            stopSong()
        }
        isMediaPlayerRunning = false
    }

    // creating a notification for the foreground service
    private fun createNotification(): Notification {

        val actionIntentPlay = Intent(this, MediaPlayerService::class.java)
        actionIntentPlay.action = ACTION_PLAY
        val actionPendingIntentPlay = PendingIntent.getService(this, 0, actionIntentPlay, 0)

        val actionIntentPause = Intent(this, MediaPlayerService::class.java)
        actionIntentPause.action = ACTION_PAUSE
        val actionPendingIntentPause = PendingIntent.getService(this, 0, actionIntentPause, 0)

        val actionIntentStop = Intent(this, MediaPlayerService::class.java)
        actionIntentStop.action = ACTION_STOP
        val actionPendingIntentStop = PendingIntent.getService(this, 0, actionIntentStop, 0)

        val actionIntentExit = Intent(this, MediaPlayerService::class.java)
        actionIntentExit.action = ACTION_EXIT
        val actionPendingIntentExit = PendingIntent.getService(this, 0, actionIntentExit, 0)

        val builder = NotificationCompat.Builder(this, channelID)
                .setContentTitle(title)
                .setContentText((mediaPlayer.currentPosition/1000).toString()+"/"+(mediaPlayer.duration/1000).toString())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setChannelId(channelID)
                .setOnlyAlertOnce(true)
                .addAction(android.R.drawable.ic_media_play, "Play", actionPendingIntentPlay)
                .addAction(android.R.drawable.ic_media_pause, "Pause", actionPendingIntentPause)
                .addAction(android.R.drawable.ic_media_ff, "Stop", actionPendingIntentStop)
                .addAction(android.R.drawable.ic_media_rew, "Exit", actionPendingIntentExit)
        val resultIntent = Intent(this, MainActivity::class.java)
        val resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent,
            PendingIntent.FLAG_UPDATE_CURRENT)
        builder.setContentIntent(resultPendingIntent)
        return builder.build()
    }

    // creating a notification channel for the foreground service
    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT < 26) {
            return
        } else {
            val channel = NotificationChannel(channelID, getString(R.string.channel_name), NotificationManager.IMPORTANCE_LOW)
            channel.description = getString(R.string.channel_desc)
            channel.enableLights(true)
            channel.lightColor = Color.RED
            channel.enableVibration(true)
            val managerCompat = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            managerCompat.createNotificationChannel(channel)
        }
    }
}