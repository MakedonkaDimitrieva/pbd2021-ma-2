package si.uni_lj.fri.pbd.miniapp2

import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.*
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import si.uni_lj.fri.pbd.miniapp2.MediaPlayerService.Companion.ACTION_START
import kotlin.system.exitProcess

class MainActivity : AppCompatActivity() {

    companion object {
        val TAG = MainActivity::class.simpleName
        // message type for the handler
        private const val MSG_UPDATE_TIME = 1
        private const val UPDATE_RATE_MS = 1000L
    }

    private val processingScope = CoroutineScope(Dispatchers.IO)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    // creating mediaPlayerService and serviceBound
    var mediaPlayerService: MediaPlayerService? = null
    var serviceBound: Boolean = false

    // handler to update the UI every second when the media player is running
    private val updateTimeHandler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            if(MSG_UPDATE_TIME == msg.what) {
                Log.d(TAG, "Updating time")
                updateUITime()
                sendEmptyMessageDelayed(MSG_UPDATE_TIME, UPDATE_RATE_MS)
            }
        }
    }

    // defining a ServiceConnection
    private val mConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            Log.d(TAG, "Service bound")
            val binder = iBinder as MediaPlayerService.RunServiceBinder
            mediaPlayerService = binder.service
            serviceBound = true
            mediaPlayerService?.background()
            // Update the UI if the service is already running the timer
            if (mediaPlayerService?.isMediaPlayerRunning == true) {
                updateUIStartRun()
                initProgressBar()
            }
        }
        override fun onServiceDisconnected(componentName: ComponentName) {
            Log.d(TAG, "Service disconnect")
            serviceBound = false
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "Starting and binding service")
        val i = Intent(this, MediaPlayerService::class.java)
        i.action = ACTION_START
        startService(i)
        bindService(i, mConnection, 0)
    }

    override fun onStop() {
        super.onStop()
        // unbinding the service if it is bound
        if(serviceBound) {
            if(mediaPlayerService?.isMediaPlayerRunning!!) {
                mediaPlayerService?.foreground()
            }
            else {
                stopService(Intent(this, MediaPlayerService::class.java))
            }
            unbindService(mConnection)
            serviceBound = false
        }
    }

    // updating the time in the UI
    @SuppressLint("SetTextI18n")
    private fun updateUITime() {
        if(serviceBound) {
            duration_information.text = (mediaPlayerService!!.mediaPlayer.currentPosition/1000).toString()+"/"+(mediaPlayerService!!.mediaPlayer.duration/1000).toString()
        }
    }

    // updating the UI when a run starts
    private fun updateUIStartRun() {
        track_title.text = mediaPlayerService?.title
        updateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME)
    }

    // updating the UI when a run stops
    @SuppressLint("SetTextI18n")
    private fun updateUIStopRun() {
        updateTimeHandler.removeMessages(MSG_UPDATE_TIME)
        track_title.text = "Track title"
        duration_information.text = "Duration information"
    }

    // initializing progress bar
    private fun initProgressBar() {
        progressBar.max = mediaPlayerService!!.mediaPlayer.duration
        updateProgressBar()
    }

    // updating progress bar
    private fun  updateProgressBar() {
        processingScope.launch {
            var progress = mediaPlayerService!!.mediaPlayer.currentPosition
            while(progress<mediaPlayerService!!.mediaPlayer.duration || !mediaPlayerService!!.pause) {
                progress = mediaPlayerService!!.mediaPlayer.currentPosition
                progressBar.progress = progress
                delay(1000)
            }
        }
    }

    fun play(v: View){
        if(serviceBound) {
            mediaPlayerService?.playSong()
            updateUIStartRun()
            initProgressBar()
        }
    }

    fun pause(v: View) {
        if(serviceBound) {
            mediaPlayerService?.pauseSong()
        }
    }

    fun stop(v: View) {
        if(serviceBound) {
            mediaPlayerService?.stopSong()
            updateUIStopRun()
        }
    }

    fun exit(v: View) {
        if(serviceBound) {
            mediaPlayerService?.exit()
        }
        exitProcess(0)
    }
}